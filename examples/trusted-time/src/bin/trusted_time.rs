extern crate sgx_types;
extern crate sgx_urts;

use sgx_types::error::SgxStatus;
use sgx_types::types::*;
use sgx_urts::enclave::SgxEnclave;
use std::ffi::CString;

static ENCLAVE_FILE: &str = "enclave.signed.so";

extern "C" {
    fn send_nts_request(
        eid: EnclaveId,
        retval: *mut SgxStatus,
        hostname: *const c_char,
        timestamp: *mut u64,
    ) -> SgxStatus;
}

fn main() {
    let enclave = match SgxEnclave::create(ENCLAVE_FILE, true) {
        Ok(enclave) => {
            println!("[+] Init Enclave Successful {}!", enclave.eid());
            enclave
        }
        Err(err) => {
            println!("[-] Init Enclave Failed {}!", err.as_str());
            return;
        }
    };

    let hostname = "time.cloudflare.com";
    let c_hostname = CString::new(hostname.to_string()).unwrap();

    let mut retval = SgxStatus::Success;
    let mut timestamp: u64 = 0;
    let result = unsafe {
        send_nts_request(
            enclave.eid(),
            &mut retval,
            c_hostname.as_ptr(),
            &mut timestamp,
        )
    };
    match result {
        SgxStatus::Success => println!("[+] ECall Success..."),
        _ => println!("[-] ECall Enclave Failed {}!", result.as_str()),
    }
    println!("Timestamp: {}", timestamp);
}
