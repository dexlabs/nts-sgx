// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License..
#[cfg(target_vendor = "teaclave")]
extern crate sgx_types;

use nts_client::{run_nts_ke_client, run_nts_ntp_client};
use sgx_types::error::SgxStatus;
use sgx_types::types::c_char;
use std::ffi::CStr;

const DEFAULT_NTP_PORT: u16 = 123;
const DEFAULT_KE_PORT: u16 = 4460;

/// # Safety
#[no_mangle]
pub unsafe extern "C" fn send_nts_request(
    hostname: *const c_char,
    timestamp: &mut u64,
) -> SgxStatus {
    let hostname = CStr::from_ptr(hostname).to_str().unwrap();
    match run_nts_ke_client(hostname, DEFAULT_KE_PORT, DEFAULT_NTP_PORT) {
        Ok(state) => {
            // Get first NTS timestamp
            let nts_res = run_nts_ntp_client(&state).unwrap();
            *timestamp = nts_res.timestamp;
            SgxStatus::Success
        }
        Err(e) => {
            println!("Cannot reach NTS KE server: {} with error {}!", hostname, e);
            SgxStatus::Unexpected
        }
    }
}
