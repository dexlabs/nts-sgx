// This file is part of cfnts.
// Copyright (c) 2019, Cloudflare. All rights reserved.
// See LICENSE for licensing information.
use std::vec::Vec;
/// Key id for `KeyRotator`.
// This struct should be `Clone` and `Copy` because the internal representation is just a `u32`.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct KeyId(u32);

#[cfg(test)]
impl KeyId {
    /// Create `KeyId` from raw `u32`.
    #[allow(unused)]
    pub fn new(key_id: u32) -> KeyId {
        KeyId(key_id)
    }

    /// Create `KeyId` from a `u64` epoch. The 32 most significant bits of the parameter will be
    /// discarded.
    #[allow(unused)]
    pub fn from_epoch(epoch: u64) -> KeyId {
        // This will discard the 32 most significant bits.
        let epoch_residue = epoch as u32;
        KeyId(epoch_residue)
    }

    /// Create `KeyId` from its representation as a byte array in big endian.
    pub fn from_be_bytes(bytes: [u8; 4]) -> KeyId {
        KeyId(u32::from_be_bytes(bytes))
    }

    /// Return the memory representation of this `KeyId` as a byte array in big endian.
    pub fn to_be_bytes(self) -> [u8; 4] {
        self.0.to_be_bytes()
    }
}
#[derive(Debug, Copy, Clone, Default)]
pub struct NTSKeys {
    pub c2s: [u8; 32],
    pub s2c: [u8; 32],
}

/// Cookie key.
#[derive(Clone, Debug)]
pub struct CookieKey(Vec<u8>);

impl CookieKey {
    /// Return a byte slice of a cookie key content.
    #[allow(unused)]
    pub fn as_bytes(&self) -> &[u8] {
        self.0.as_slice()
    }
}

// Only used in test.
impl From<&[u8]> for CookieKey {
    fn from(bytes: &[u8]) -> CookieKey {
        CookieKey(Vec::from(bytes))
    }
}
