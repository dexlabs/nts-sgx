#[cfg(target_vendor = "teaclave")]
extern crate sgx_libc;
#[cfg(target_vendor = "teaclave")]
extern crate sgx_oc;
#[cfg(target_vendor = "teaclave")]
extern crate sgx_trts;

#[cfg(target_vendor = "teaclave")]
mod exception;
mod protocol;
mod records;

use self::NtpClientError::*;
#[cfg(target_vendor = "teaclave")]
use crate::exception::ExceptionHandler;
use aes_siv::{aead::NewAead, Aes128SivAead};
use anyhow::{anyhow, Result};
use protocol::{
    parse_nts_packet, serialize_nts_packet, LeapState, NtpExtension, NtpExtensionType::*,
    NtpPacketHeader, NtsPacket, PacketMode::Client, TWO_POW_32, UNIX_OFFSET,
};
use rand::Rng;
use records::{
    deserialize,
    gen_key,
    process_record,
    // Functions.
    serialize,
    // Records.
    AeadAlgorithmRecord,
    EndOfMessageRecord,
    // Enums.
    KnownAeadAlgorithm,
    KnownNextProtocol,
    NTSKeys,
    NextProtocolRecord,
    Party,
    // Structs.
    ReceivedNtsKeRecordState,
    // Constants.
    HEADER_SIZE,
};
use std::{
    io::{Read, Write},
    net::{Shutdown, TcpStream, ToSocketAddrs, UdpSocket},
    string::String,
    sync::Arc,
    time::{Duration, SystemTime},
    vec::Vec,
};
use thiserror::Error;
use webpki;
use webpki_roots;
const BUFF_SIZE: usize = 2048;
const TCP_TIMEOUT: Duration = Duration::from_secs(1);

pub struct NtpResult {
    pub stratum: u8,
    /// Time difference from the NTS clock and local system clock (sec).
    pub time_diff: f64,
    /// Unix epoch timestamp in seconds
    pub timestamp: u64,
}

#[derive(Debug, Clone, Error)]
pub enum NtpClientError {
    #[error("Ntp Client Error: No IPv4 address found")]
    NoIpv4AddrFound,
    #[error("Ntp Client Error: No IPv6 address found")]
    NoIpv6AddrFound,
    #[error("Ntp Client Error: Invalid UID")]
    InvalidUid,
}

const DEFAULT_SCHEME: u16 = 0;
const TIMEOUT: Duration = Duration::from_secs(15);

type Cookie = Vec<u8>;

#[derive(Clone, Debug, Default)]
pub struct NtsKeResult {
    pub cookies: Vec<Cookie>,
    pub next_protocols: Vec<u16>,
    pub aead_scheme: u16,
    pub next_server: String,
    pub next_port: u16,
    pub keys: NTSKeys,
    pub use_ipv4: Option<bool>,
}

/// Returns a float representing the system time as NTP
fn system_to_ntp_float(time: SystemTime) -> f64 {
    let unix_time = time.duration_since(SystemTime::UNIX_EPOCH).unwrap(); // Safe absent time machines
    let unix_offset = Duration::new(UNIX_OFFSET, 0);
    let epoch_time = unix_offset + unix_time;
    epoch_time.as_secs() as f64 + (epoch_time.subsec_nanos() as f64) / 1.0e9
}

/// Returns a float representing the ntp timestamp
fn timestamp_to_float(time: u64) -> f64 {
    let ts_secs = time >> 32;
    let ts_frac = time - (ts_secs << 32);
    (ts_secs as f64) + (ts_frac as f64) / TWO_POW_32
}

pub fn run_nts_ke_client(nts_hostname: &str, ke_port: u16, ntp_port: u16) -> Result<NtsKeResult> {
    // Prepare tls client
    let mut tls_config = rustls::ClientConfig::new();
    let protocol = String::from("ntske/1");
    let protocol_bytes = protocol.into_bytes();
    tls_config.set_protocols(&[protocol_bytes]);
    tls_config
        .root_store
        .add_server_trust_anchors(&webpki_roots::TLS_SERVER_ROOTS);
    let rc_config = Arc::new(tls_config);
    #[cfg(target_vendor = "teaclave")]
    let _handle = ExceptionHandler::new()?;
    let hostname = webpki::DNSNameRef::try_from_ascii_str(nts_hostname)?;
    let mut client = rustls::ClientSession::new(&rc_config, hostname);
    // Get tcp socket for nts connections
    let sock_addr = (nts_hostname, ke_port).to_socket_addrs()?.next().unwrap();
    let mut sock = TcpStream::connect_timeout(&sock_addr, TCP_TIMEOUT)?;
    let mut tls_stream = rustls::Stream::new(&mut client, &mut sock);
    let next_protocol_record = NextProtocolRecord::from(vec![KnownNextProtocol::Ntpv4]);
    let aead_record = AeadAlgorithmRecord::from(vec![KnownAeadAlgorithm::AeadAesSivCmac256]);
    let end_record = EndOfMessageRecord;

    let client_rec = &mut serialize(next_protocol_record);
    client_rec.append(&mut serialize(aead_record));
    client_rec.append(&mut serialize(end_record));
    tls_stream.write_all(client_rec)?;
    tls_stream.flush()?;
    let keys = gen_key(tls_stream.sess)?;
    let mut state = ReceivedNtsKeRecordState {
        finished: false,
        next_protocols: Vec::new(),
        aead_scheme: Vec::new(),
        cookies: Vec::new(),
        next_server: None,
        next_port: None,
    };

    while !state.finished {
        let mut header: [u8; HEADER_SIZE] = [0; HEADER_SIZE];

        // We should use `read_exact` here because we always need to read 4 bytes to get the
        // header.
        tls_stream.read_exact(&mut header[..])?;

        // Retrieve a body length from the 3rd and 4th bytes of the header.
        let body_length = u16::from_be_bytes([header[2], header[3]]);
        let mut body = vec![0; body_length as usize];

        // `read_exact` the length of the body.
        tls_stream.read_exact(body.as_mut_slice())?;

        // Reconstruct the whole record byte array to let the `records` module deserialize it.
        let mut record_bytes = Vec::from(&header[..]);
        record_bytes.append(&mut body);

        // `deserialize` has an invariant that the slice needs to be long enough to make it a
        // valid record, which in this case our slice is exactly as long as specified in the
        // length field.
        let record = deserialize(Party::Client, record_bytes.as_slice())?;
        process_record(record, &mut state)?;
    }
    #[cfg(feature = "log")]
    tracing::debug!(?state, "Finish reading the key exchange record!");
    sock.shutdown(Shutdown::Write)?;
    let aead_scheme = if state.aead_scheme.is_empty() {
        DEFAULT_SCHEME
    } else {
        state.aead_scheme[0]
    };

    let state = NtsKeResult {
        aead_scheme,
        cookies: state.cookies,
        next_protocols: state.next_protocols,
        next_server: state
            .next_server
            .unwrap_or_else(|| nts_hostname.to_string()),
        next_port: state.next_port.unwrap_or(ntp_port),
        keys,
        use_ipv4: Some(true),
    };
    Ok(state)
}

/// Run the NTS client with the given data from key exchange
pub fn run_nts_ntp_client(state: &NtsKeResult) -> Result<NtpResult> {
    let mut ip_addrs = (state.next_server.as_str(), state.next_port).to_socket_addrs()?;
    let addr;
    let socket;
    if let Some(use_ipv4) = state.use_ipv4 {
        if use_ipv4 {
            // mandated to use ipv4
            addr = ip_addrs.find(|&x| x.is_ipv4());
            if addr.is_none() {
                return Err(NoIpv4AddrFound.into());
            }
            socket = UdpSocket::bind("0.0.0.0:0");
        } else {
            // mandated to use ipv6
            addr = ip_addrs.find(|&x| x.is_ipv6());
            if addr.is_none() {
                return Err(NoIpv6AddrFound.into());
            }
            socket = UdpSocket::bind("[::]:0");
        }
    } else {
        // sniff whichever one is supported
        addr = ip_addrs.next();
        // check if this address is ipv4 or ipv6
        if addr.unwrap().is_ipv6() {
            socket = UdpSocket::bind("[::]:0");
        } else {
            socket = UdpSocket::bind("0.0.0.0:0");
        }
    }
    let socket = socket.expect("Cannot create an udp socket!");
    socket.set_read_timeout(Some(TIMEOUT))?;
    socket.set_write_timeout(Some(TIMEOUT))?;
    let mut send_aead = Aes128SivAead::new_from_slice(&state.keys.c2s)
        .map_err(|e| anyhow!("Create client-to-server encryption key error: {:?}", e))?;
    let mut recv_aead = Aes128SivAead::new_from_slice(&state.keys.s2c)
        .map_err(|e| anyhow!("Create server-to-client encryption key error: {:?}", e))?;
    let header = NtpPacketHeader {
        leap_indicator: LeapState::NoLeap,
        version: 4,
        mode: Client,
        stratum: 0,
        poll: 0,
        precision: 0x20,
        root_delay: 0,
        root_dispersion: 0,
        reference_id: 0,
        reference_timestamp: 0xdeadbeef,
        origin_timestamp: 0,
        receive_timestamp: 0,
        transmit_timestamp: 0,
    };
    let mut unique_id: Vec<u8> = vec![0; 32];
    rand::thread_rng().fill(&mut unique_id[..]);
    let exts = vec![
        NtpExtension {
            ext_type: UniqueIdentifier,
            contents: unique_id.clone(),
        },
        NtpExtension {
            ext_type: NTSCookie,
            contents: state.cookies[0].clone(),
        },
    ];
    let packet = NtsPacket {
        header,
        auth_exts: exts,
        auth_enc_exts: vec![],
    };
    socket.connect(addr.unwrap())?;
    let wire_packet = &serialize_nts_packet::<Aes128SivAead>(packet, &mut send_aead);
    let t1 = system_to_ntp_float(SystemTime::now());
    socket.send(wire_packet)?;
    #[cfg(feature = "log")]
    tracing::debug!("transmitting NTS packet");
    let mut buff = [0; BUFF_SIZE];
    let (size, _origin) = socket.recv_from(&mut buff)?;
    let t4 = system_to_ntp_float(SystemTime::now());
    #[cfg(feature = "log")]
    tracing::debug!("received NTS packet");
    let packet = parse_nts_packet::<Aes128SivAead>(&buff[0..size], &mut recv_aead)?;
    // check if server response contains the same UniqueIdentifier as client request
    let resp_unique_id = packet.auth_exts[0].clone().contents;
    if resp_unique_id != unique_id {
        return Err(InvalidUid.into());
    }
    Ok(NtpResult {
        stratum: packet.header.stratum,
        time_diff: ((timestamp_to_float(packet.header.receive_timestamp) - t1)
            + (timestamp_to_float(packet.header.transmit_timestamp) - t4))
            / 2.0,
        timestamp: (packet.header.transmit_timestamp >> 32) - 2208988800,
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    use aes_siv::{
        aead::{Aead, NewAead},
        Aes128SivAead,
    };
    use core::convert::TryInto;
    use protocol::{decrypt, encrypt, parse_packet_header, serialize_header, PacketMode::*};
    use rand::Rng;
    use records::KeyId;
    use std::{
        thread,
        time::{Duration, SystemTime},
    };
    const COOKIE_SIZE: usize = 100;

    #[test]
    fn test_system_time_to_float() {
        // Test the time conversion from system time to float-point represented epoch time in seconds.
        let t1 = system_to_ntp_float(SystemTime::now());
        let elapse_time = Duration::from_secs(1);
        thread::sleep(elapse_time);
        let t2 = system_to_ntp_float(SystemTime::now());
        assert_eq!((t2 - t1).round(), 1.0)
    }

    fn make_cookie(keys: NTSKeys, master_key: &[u8], key_id: KeyId) -> Vec<u8> {
        let mut nonce = [0; 16];
        rand::thread_rng().fill(&mut nonce);
        let mut plaintext = [0; 64];
        plaintext[..32].copy_from_slice(&keys.c2s[..32]);
        plaintext[32..(32 + 32)].copy_from_slice(&keys.s2c[..32]);
        let mut aead = Aes128SivAead::new_from_slice(master_key).unwrap();
        let mut ciphertext = encrypt(&mut aead, &nonce, &[], &plaintext).unwrap();
        let mut out = Vec::new();
        out.extend(key_id.to_be_bytes());
        out.extend(nonce);
        out.append(&mut ciphertext);
        out
    }

    fn unpack(pt: Vec<u8>) -> Option<NTSKeys> {
        if pt.len() != 64 {
            None
        } else {
            let mut key = NTSKeys {
                c2s: [0; 32],
                s2c: [0; 32],
            };
            key.c2s[..32].copy_from_slice(&pt[..32]);
            key.s2c[..32].copy_from_slice(&pt[32..(32 + 32)]);
            Some(key)
        }
    }

    fn eat_cookie(cookie: &Vec<u8>, key: &[u8]) -> Option<NTSKeys> {
        if cookie.len() < 40 {
            return None;
        }
        let ciphertext = &cookie[4..];
        let mut aead = Aes128SivAead::new_from_slice(key).unwrap();
        let answer = decrypt(&mut aead, &ciphertext[0..16], &[], &ciphertext[16..]);
        match answer {
            Err(_) => None,
            Ok(buf) => unpack(buf),
        }
    }

    fn check_eq(a: NTSKeys, b: NTSKeys) {
        for i in 0..32 {
            assert_eq!(a.c2s[i], b.c2s[i]);
            assert_eq!(a.s2c[i], b.s2c[i]);
        }
    }

    fn get_key_id(cookie: &[u8]) -> Option<KeyId> {
        if cookie.len() < 4 {
            None
        } else {
            Some(KeyId::from_be_bytes((&cookie[0..4]).try_into().unwrap()))
        }
    }

    #[test]
    fn test_nts_cookie() {
        let test = NTSKeys {
            s2c: [9; 32],
            c2s: [10; 32],
        };

        let master_key = [0x07; 32];
        let key_id = KeyId::from_be_bytes([0x03; 4]);
        let mut cookie = make_cookie(test, &master_key, key_id);
        let ret = get_key_id(&cookie);
        assert_eq!(cookie.len(), COOKIE_SIZE);
        match ret {
            None => panic!("Cannot get NTS key ID!"),
            Some(id) => assert_eq!(id, key_id),
        }

        let ret2 = eat_cookie(&cookie, &master_key);
        match ret2 {
            None => panic!("Cannot get NTS keys!"),
            Some(new_key) => check_eq(new_key, test),
        }

        cookie[9] = 0xff;
        cookie[10] = 0xff;
        let ret3 = eat_cookie(&cookie, &master_key);
        match ret3 {
            None => (),
            Some(_) => panic!("No keys shall be generated with wrong nonce!"),
        }
    }

    #[test]
    fn test_ntp_header_parse() {
        let leaps = vec![
            LeapState::NoLeap,
            LeapState::Positive,
            LeapState::Negative,
            LeapState::Unknown,
        ];
        let versions = vec![1, 2, 3, 4, 5, 6, 7];
        let modes = vec![SymmetricActive, SymmetricPassive, Client, Server, Broadcast];
        for leap in &leaps {
            for version in &versions {
                for mode in &modes {
                    let start_header = NtpPacketHeader {
                        leap_indicator: *leap,
                        version: *version,
                        mode: *mode,
                        stratum: 0,
                        poll: 0,
                        precision: 0,
                        root_delay: 0,
                        root_dispersion: 0,
                        reference_id: 0,
                        reference_timestamp: 0,
                        origin_timestamp: 0,
                        receive_timestamp: 0,
                        transmit_timestamp: 0,
                    };
                    let ret_header = parse_packet_header(&serialize_header(start_header)).unwrap();
                    assert_eq!(ret_header, start_header)
                }
            }
        }
    }

    fn check_eq_ext(a: &NtpExtension, b: &NtpExtension) {
        assert_eq!(a.ext_type, b.ext_type);
        assert_eq!(a.contents.len(), b.contents.len());
        for i in 0..a.contents.len() {
            assert_eq!(a.contents[i], b.contents[i]);
        }
    }

    fn check_ext_array_eq(exts1: Vec<NtpExtension>, exts2: Vec<NtpExtension>) {
        assert_eq!(exts1.len(), exts2.len());
        for i in 0..exts1.len() {
            check_eq_ext(&exts1[i], &exts2[i]);
        }
    }

    fn check_nts_match(pkt1: NtsPacket, pkt2: NtsPacket) {
        assert_eq!(pkt1.header, pkt2.header);
        check_ext_array_eq(pkt1.auth_enc_exts, pkt2.auth_enc_exts);
        check_ext_array_eq(pkt1.auth_exts, pkt2.auth_exts);
    }

    fn roundtrip_test<T: Aead>(input: NtsPacket, enc: &mut T) {
        let mut packet = serialize_nts_packet::<T>(input.clone(), enc);
        let decrypt = parse_nts_packet(&packet, enc).unwrap();
        check_nts_match(input, decrypt);
        packet[0] = 0xde;
        packet[1] = 0xad;
        packet[2] = 0xbe;
        packet[3] = 0xef;
        let failure = parse_nts_packet(&packet, enc);
        if failure.is_ok() {
            panic!("success when we should have failed");
        }
    }

    #[test]
    fn test_nts_parse() {
        let key = [0; 32];
        let mut test_aead = Aes128SivAead::new((&key).into());
        let packet_header = NtpPacketHeader {
            leap_indicator: LeapState::NoLeap,
            version: 4,
            mode: Client,
            stratum: 1,
            poll: 0,
            precision: 0,
            root_delay: 0,
            root_dispersion: 0,
            reference_id: 0,
            reference_timestamp: 0,
            origin_timestamp: 0,
            receive_timestamp: 0,
            transmit_timestamp: 0,
        };

        let packet = NtsPacket {
            header: packet_header,
            auth_exts: vec![
                NtpExtension {
                    ext_type: UniqueIdentifier,
                    contents: vec![0; 32],
                },
                NtpExtension {
                    ext_type: NTSCookie,
                    contents: vec![0; 32],
                },
            ],
            auth_enc_exts: vec![NtpExtension {
                ext_type: NTSCookiePlaceholder,
                contents: vec![0xfe; 32],
            }],
        };
        roundtrip_test::<Aes128SivAead>(packet, &mut test_aead);
    }
}
