# NTS-SGX: A NTS client for Intel SGX

## Description
In secure systems, time plays a crucial role in validating security properties. Therefore, safeguarding the integrity of time information is essential. Intel SGX allows the creation of secure applications within a Trusted Execution Environment (TEE) known as an enclave, which is isolated from the untrusted operating system (OS). However, retrieving time information from the enclave remains challenging due to the OS’s control over system time. In earlier versions of the SGX SDK, the sgx_get_trusted_time function served as an alternative to OS time.
However, Intel removed the API from Linux in 2020, without providing an alternative.

There are [other solutions] (https://dl.acm.org/doi/10.1007/978-3-031-39828-5_17) based on the TPM chips, which requires a specific hardware to create trusted timestamp.

We ported the client side implementation from CloudFlare's [Rust implementation](https://github.com/cloudflare/cfnts) as a light-weight solution for retrieving timestamp from one or many trusted time servers.

Using this library with a secured enclave, the time information is protected from attacks by untrusted system operators or man-in-the-middle attacks.

## Usage
This crate is only compatible with [DEX Labs's fork of Teaclave Rust SGX SDK](https://gitlab.com/dexlabs/incubator-teaclave-sgx-sdk)

The `nts-client` module can be compiled to both regular x86 Linux target and SGX target.

We provide an example under [examples](./examples/). Use `make run` to build and run the example. The Intel SGX SDK 2.21 is required to build this example. 

Please use the below docker image to build and run the example if you have dependency issues.
```shell
docker pull yangfh2004/sgx-rust:22.04-1.76-nightly
```

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Contributing
You are welcome to make contributions to this repo and raise an issue.

## Authors and acknowledgment
This repo is based on the implementation of NTS from Cloudflare and modified for the SGX enclave.

## License
BSD 2-Clause

